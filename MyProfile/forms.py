from django import forms


class Message_Form(forms.Form):

    error_messages = {
        'required': 'This field is required.',
        'invalid': 'Invalid input!',
        'blank' : 'This field is required.'
        
    }
    attrs = {
        'class': 'form-control',
        'size': 100
    }

    day = forms.CharField(label='Day', required=True, widget=forms.TextInput(attrs=attrs))
    date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs={'type' : 'date'}))
    time = forms.TimeField(label='Time', required=True, widget=forms.TimeInput(attrs={'type' : 'time'}))
    activity = forms.CharField(label='Activity', required=True, widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label='Place', required=True, widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='Category', required=True,  widget=forms.TextInput(attrs=attrs))
    
