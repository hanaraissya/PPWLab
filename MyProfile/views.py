from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse 
from django.views.decorators.csrf import csrf_exempt
from .models import fillSchedule
from .forms import Message_Form
from datetime import datetime
import requests
from .models import addGuest

# Create your views here.
response = {'author' : 'HanaRaissya'}
API_BUKU = "https://www.googleapis.com/books/v1/volumes?q=quilting"
def index(request):
    return render(request, 'index.html')
def experience(request):
    return render(request, 'experience.html')
def discoverme(request):
    return render(request, 'discoverme.html')
# def form(request):
#     return render(request, 'form.html')
def add_schedule(request):
	response['message_form'] = Message_Form
	form = Message_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['day'] = request.POST['day'] 
		response['date'] = request.POST['date'] 
		response['time'] = request.POST['time'] 
		response['activity'] = request.POST['activity'] 
		response['place'] = request.POST['place']
		response['category'] = request.POST['category']
		message = fillSchedule(day=response['day'], date=response['date'], time=response['time'], activity=response['activity'], place=response['place'], category=response['category'])
		message.save()
		return schedule_result(request)
	else:
		response['form_message'] = form
		html = 'schedule.html'
		return render(request, html, response)

def schedule_result(request):
	result = fillSchedule.objects.all()
	response['form_result'] = result
	htmls = 'schedule_result.html'
	return render(request, htmls, response)

def schedule_delete(request):
	fillSchedule.objects.all().delete()
	return HttpResponseRedirect("../schedule_result")

def story9(request):
    return render(request, 'story9.html',response)

def data_buku(request):
	adds = request.GET.get('k', 'quilting')
	url = "https://www.googleapis.com/books/v1/volumes?q=" + adds
	data = requests.get(url).json()
	return JsonResponse(data)

@csrf_exempt
def add_guest(request):
    if request.method == "POST":
        data = dict(request.POST.items())
        guest = addGuest(name = data['name'], email = data['email'], password = data['password'])
        try:
            guest.full_clean()
            guest.save()
        except Exception as e:
            e = [v[0] for k, v in e.message_dict.items()]
            return JsonResponse({"error": e}, status=400)
        return JsonResponse({"message": "Data berhasil disimpan"})
    else:
        return render(request, 'form.html', {})

@csrf_exempt
def checkEmail(request):
    email = request.GET["email"]
    emails = addGuest.objects.filter(email=email).all()
    return JsonResponse({"email": email, "available": not bool(emails)})






