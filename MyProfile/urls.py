from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from .views import index
from .views import experience
from .views import discoverme
# from .views import form
from .views import add_schedule
from .views import schedule_result
from .views import schedule_delete
from .views import story9
from .views import data_buku
from .views import add_guest
from .views import checkEmail


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', index, name = 'index'),
    url(r'experience', experience, name = 'experience'),
    url(r'discoverme', discoverme, name = 'discoverme'),
    # url(r'^form', form, name = 'form'),
    url(r'index', index, name = 'index'),
    url(r'add_schedule', add_schedule, name = 'add_schedule'),
    url(r'schedule_result', schedule_result, name = 'schedule_result'),
    url(r'schedule_delete', schedule_delete, name = 'schedule_delete'),
    url(r'story9', story9, name = 'story9'),
    url(r'buku', data_buku, name = 'data_buku'),
    url(r'guest', add_guest, name = 'add_guest'),
    url(r'checkEmail', checkEmail, name = 'checkEmail'),
    ]
