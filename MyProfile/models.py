from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class fillSchedule(models.Model):
	day = models.CharField(max_length=100)
	date = models.DateField()
	time = models.TimeField()
	activity = models.CharField(max_length=100)
	place = models.CharField(max_length=100)
	category = models.CharField(max_length=100)

class addGuest(models.Model):
	name = models.CharField(max_length=100)
	email = models.EmailField(max_length=200, unique= True)
	password = models.CharField(max_length=30)