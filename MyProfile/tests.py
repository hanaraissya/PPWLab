from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, experience, discoverme, add_schedule, schedule_result, schedule_delete, add_guest, checkEmail
from .models import fillSchedule, addGuest
from .forms import Message_Form
import datetime
# Create your tests here.
class MyProfileUnitTest(TestCase):
    
    def test_MyProfile_url_index_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_MyProfile_using_index_func(self):
      found = resolve('/')
      self.assertEqual(found.func, index)

    def test_MyProfile_url_experience_is_exist(self):
        response = Client().get('/experience')
        self.assertEqual(response.status_code,200)

    def test_MyProfile_using_experience_func(self):
      found = resolve('/experience')
      self.assertEqual(found.func, experience)

    def test_MyProfile_url_discoverme_is_exist(self):
        response = Client().get('/discoverme')
        self.assertEqual(response.status_code,200)

    def test_MyProfile_using_discoverme_func(self):
      found = resolve('/discoverme')
      self.assertEqual(found.func, discoverme)

    # def test_MyProfile_url_form_is_exist(self):
    #     response = Client().get('/form')
    #     self.assertEqual(response.status_code,200)

    # def test_MyProfile_using_form_func(self):
    #   found = resolve('/form')
    #   self.assertEqual(found.func, form)

    def test_model_can_fill_schedule(self):
    	new_schedule = fillSchedule.objects.create(day='senin', date= datetime.date(2010, 1, 1) , time= datetime.time(12, 12) , activity='Hmm', place='Hmm', category='Hmm' )
    	counting_all_available_status = fillSchedule.objects.all().count()
    	self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
    	form = Message_Form(data={'day':'', 'date': '' , 'time':'' , 'activity':'', 'place':'', 'category':''})
    	self.assertFalse(form.is_valid())
    	self.assertEqual(form.errors['day'], ["This field is required."] )

    def test_schedule_post_success_and_render_the_result(self):
      test = 'Anonymous'
      test2 = datetime.date(2010, 1, 1)
      test3 = datetime.time(12, 12)
      response_post = Client().post('/add_schedule', {'day': test, 'date': test2 , 'time': test3 , 'activity': test, 'place': test , 'category': test })
      self.assertEqual(response_post.status_code, 200)

      response= Client().get('/schedule_result')
      html_response = response.content.decode('utf8')
      self.assertIn(test, html_response)

    def test_schedule_post_error_and_render_the_result(self):
      test = 'Anonymous'
      response_post = Client().post('/add_schedule', {'day':'', 'date': '' , 'time':'' , 'activity':'', 'place':'', 'category':''})
      self.assertEqual(response_post.status_code, 200)

      response= Client().get('/add_schedule')
      html_response = response.content.decode('utf8')
      self.assertNotIn(test, html_response)

    def test_MyProfile_url_schedule_result_is_exist(self):
        response = Client().get('/schedule_result')
        self.assertEqual(response.status_code,200)

    def test_MyProfile_url_schedule_delete_is_exist(self):
    	response = Client().get('/schedule_delete')
    	self.assertEqual(response.status_code,302)

    	response= Client().get('/schedule_result')
    	self.assertEqual(response.status_code,200)

    def test_MyProfile_using_schedule_result_func(self):
    	found = resolve('/schedule_delete')
    	self.assertEqual(found.func, schedule_delete)

    def test_story9_url_exists(self):
      response = Client().get('/story9/')
      self.assertEqual(response.status_code, 200)

    def test_json_data_buku_url_exists(self):
      response = Client().get('/buku/')
      self.assertEqual(response.status_code, 200)

    def test_story10_regis_url_exists(self):
      response = Client().get('/guest/')
      self.assertEqual(response.status_code, 200)

    def test_story10_using_register_func(self):
      found = resolve('/add_guest')
      self.assertEqual(found.func, add_guest)

    def test_story10_email_url_exists(self):
      response = Client().get('/checkEmail/?email=testtest@gmail.com/')
      self.assertEqual(response.status_code, 200)

    def test_story10_using_email_func(self):
      found = resolve('/checkEmail/?email=testtest@gmail.com')
      self.assertEqual(found.func, checkEmail)

    def test_story10_post_success_and_render_the_result(self):
      test = 'Anonymous'
      response_post = Client().post('/guest', {'name': 'test', 'email' : 'testtest@gmail.com', 'password' : 'test'})
      self.assertEqual(response_post.status_code, 200)

    def test_story10_post_fail_and_render_the_result(self):
      test = 'Anonymous'
      response_post = Client().post('/guest', {'name': '', 'email' : '', 'password' : ''})
      self.assertEqual(response_post.status_code, 400)

    def test_story10_model_can_create_new_registered(self):
      new_registered = addGuest.objects.create(name='test', email = 'testtest@gmail.com', password='test')
      counting_all_registered = addGuest.objects.all().count()
      self.assertEqual(counting_all_registered, 1)
