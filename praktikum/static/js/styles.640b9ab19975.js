var counter = 0;
var acc = document.getElementsByClassName("accordion");
var i;
var myVar;

// Story9
function countStar(id){
    var star = $('#'+id).html();
    if(star.includes("gray")) {
        counter++;
        $('#'+id).html("<i class='fa fa-star' style = 'color : blue'></i>");
        $("#checkFav").html("<i class='fa fa-star'style = 'color : blue'></i> " +counter + " Buku favorit");
    }
    else{
        counter--;
        $('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
        if(counter == 0){
            $("#checkFav").html("<i class='fa fa-star'style = 'color : blue'></i>  Favorit saya");
        }else{
        $("#checkFav").html("<i class='fa fa-star'style = 'color : blue'></i> " +counter + " Buku favorit");
        }
    }   
}

$(document).ready(function(){
    // Story9 
    $.ajax({
        url: "/buku/",
        success: function(result){
            result = result.items
            var hdr = "<thead><tr><th>Judul</th><th>Penulis</th><th>Cover</th><th>Deskripsi</th><th></th></tr></thead>";
                $("#btable").append(hdr);
                $("#btable").append("<tbody>");
            for(i=0; i<result.length; i++){
                var tmp = "<tr class = 'bukubuku'><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>" +"<img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'>" + "</td><td>" + result[i].volumeInfo.description + "</td><td>" + "<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+"' onclick = 'countStar("+ "\""+result[i].id+"\"" +")'> <i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>";
                $("#btable").append(tmp);
            }
            $("#btable").append("</tbody>");
        }
    });
    // MyProfile Theme
    $(".buttonblue").click(function(){$(".base").css("background-color", "#eff6ff"); 
                                $("#half").css("background", "linear-gradient(to right, #eff6ff 0%, #eff6ff 50%, #FFFFFF 50%, #FFFFFF 100%)");
                                $(".navbar").css("background-color", "#87BCDE");
                            	$(".footer").css("background-color", "#87BCDE");
                            	$("#trow").css("background-color", "#6093E2");
                            	$("#tcol").css("background-color", "#ffffff");
                            	$(".accordion").css("background-color", "#eff6ff");
    });
    $(".buttonbase").click(function(){$(".base").css("background-color", "#FBFCB0");
    							$("#half").css("background", "linear-gradient(to right, #FBFCB0 0%, #FBFCB0 50%, #FFFFFF 50%, #FFFFFF 100%)");
                                $(".navbar").css("background-color", "#EECD58");
                            	$(".footer").css("background-color", "#EECD58");
                            	$("#trow").css("background-color", "#ffd11a");
                            	$("#tcol").css("background-color", "#ffffcc");
                            	$(".accordion").css("background-color", "#FBFCB0");
    });
    
    // accordion
	for (i = 0; i < acc.length; i++) {
	  acc[i].addEventListener("click", function() {
	    this.classList.toggle("active");
	    var panel = this.nextElementSibling;
	    if (panel.style.maxHeight){
	      panel.style.maxHeight = null;
	    } else {
	      panel.style.maxHeight = panel.scrollHeight + "px";
	    } 
	  });
	}

});

// loader
function myFunction() {
    myVar = setTimeout(showPage, 500);
}
function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}