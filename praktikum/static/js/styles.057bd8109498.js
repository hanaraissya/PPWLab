
$(document).ready(function(){
    $(".buttonblue").click(function(){$(".base").css("background-color", "#eff6ff"); 
                                $("#half").css("background", "linear-gradient(to right, #eff6ff 0%, #eff6ff 50%, #FFFFFF 50%, #FFFFFF 100%)");
                                $(".navbar").css("background-color", "#87BCDE");
                            	$(".footer").css("background-color", "#87BCDE");
                            	$("#trow").css("background-color", "#6093E2");
                            	$("#tcol").css("background-color", "#ffffff");
                            	$(".accordion").css("background-color", "#eff6ff");
                            });
    $(".buttonbase").click(function(){$(".base").css("background-color", "#FBFCB0");
    							$("#half").css("background", "linear-gradient(to right, #FBFCB0 0%, #FBFCB0 50%, #FFFFFF 50%, #FFFFFF 100%)");
                                $(".navbar").css("background-color", "#EECD58");
                            	$(".footer").css("background-color", "#EECD58");
                            	$("#trow").css("background-color", "#ffd11a");
                            	$("#tcol").css("background-color", "#ffffcc");
                            	$(".accordion").css("background-color", "#FBFCB0");
                            });
    
    var acc = document.getElementsByClassName("accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
	  acc[i].addEventListener("click", function() {
	    this.classList.toggle("active");
	    var panel = this.nextElementSibling;
	    if (panel.style.maxHeight){
	      panel.style.maxHeight = null;
	    } else {
	      panel.style.maxHeight = panel.scrollHeight + "px";
	    } 
	  });
	}

});

function onReady(callback) {
  var intervalId = window.setInterval(function() {
    if (document.getElementsByTagName('body')[0] !== undefined) {
      window.clearInterval(intervalId);
      callback.call(this);
    }
  }, 1000);
}

function setVisible(selector, visible) {
  document.querySelector(selector).style.display = visible ? 'block' : 'none';
}

onReady(function() {
  setVisible('.page', true);
  setVisible('#loading', false);
});